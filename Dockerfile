FROM jfyne/node-alpine-yarn

ENV NODE_ENV=production

COPY build .
COPY keycloak.json package.json yarn.lock ./

RUN yarn install

EXPOSE 3000
CMD ["node", "service"]
