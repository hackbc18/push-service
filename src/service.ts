import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as log from 'fancy-log';
import * as helmet from 'helmet';
import * as http from 'http-error-handler';
import * as Keycloak from 'keycloak-connect';
import expressValidator = require('express-validator');

import * as api from './api';

const service = express();

config();
startService();

function config() {

  const keycloak = new Keycloak({});
  keycloak.accessDenied = (req, res) => res.status(http.forbidden.code).json(http.forbidden);

  // middleware
  service.use(helmet());
  service.use(bodyParser.json());
  service.use(bodyParser.urlencoded({ extended: true }));
  service.use(cors());
  service.use(expressValidator());
  service.use(keycloak.middleware());

  service.options('*', cors());

  service.get('/api/account', api.accountData);
  service.get('/api/address', api.newAddress);
  service.get('/api/message/:hash', api.getMessage);
  service.post('/api/message/', api.getMessageArray);
  service.get('/api/recent/:address', api.getRecentTransfers);
  service.get('/api/transfers/:address', api.getAllTransfers);
  service.get('/api/transfers', api.getTransfers);
  service.post('/api/push', api.push);

  // error handler
  service.use(http.errorHandler);
  service.use(http.notFoundHandler);
}

function startService() {
  const server = service.listen('3000', () => {
    log.info(`Service running in ${service.get('env')} mode\n`);
  });
  server.timeout = 1200000;
}

module.exports = service;
