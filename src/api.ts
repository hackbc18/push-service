import * as http from 'http-error-handler';
import * as log from 'fancy-log';
import * as request from 'request-promise-native';
import { NextFunction, Request, Response} from 'express';

const IOTA = require('iota.lib.js');
const HOST = 'http://alpaka.hahnpro.com';
const PORT = 14265;
const SEED = 'ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB';

const iota = new IOTA({
  host: HOST,
  port: PORT,
});

export let newAddress = (req: Request, res: Response, next: NextFunction) => {

  iota.api.getNewAddress(SEED, (e, address) => {
    if (!e) {
      log.info('Generated new address: ', address);

      const transfer = [{
        address: address,
        value: 0,
      }];

      iota.api.sendTransfer(SEED, 1, 14, transfer, (err, bundle) => {
        if (!err) {
          log.info('Address has been attached: ', bundle);
          return res.status(200).json({ address: address });
        }
        else {
          return res.status(500).json({ error: err });
        }
      });

    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};

export let push = (req: Request, res: Response, next: NextFunction) => {

  const { data, prevHash, inputs, address } = req.body;
  const messageToSend = { data, prevHash, inputs };
  const messageStringified = JSON.stringify(messageToSend);
  const messageTrytes = iota.utils.toTrytes(messageStringified);
  log.info(messageTrytes);

  const transfer = [{
    address: address,
    value: 0,
    message: messageTrytes,
  }];

  iota.api.sendTransfer(SEED, 1, 15, transfer, (err, bundle) => {
    if (!err) {
      log.info('Successfully sent your transfer: ', bundle);
      return res.status(200).json(bundle);
    }
    else {
      return res.status(500).json({ error: err });
    }
  });

};

export let getMessage = (req: Request, res: Response, next: NextFunction) => {

  const { hash } = req.params;

  iota.api.getTransactionsObjects([hash], (e, data) => {
    if (!e) {
      if (data.length > 0) {
        const message = iota.utils.extractJson(data);
        return res.status(200).json({ message });
      }
      else {
        return res.status(404).json({ error: 'Hash Not Found' });
      }
    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};

export let getMessageArray = (req: Request, res: Response, next: NextFunction) => {

  const { hashes } = req.body;

  iota.api.getTransactionsObjects(hashes, (e, data) => {
    if (!e) {
      if (data.length > 0) {
        let message = '';
        for (const dat of data) {
          message += iota.utils.extractJson(data);
        }
        return res.status(200).json({ message });
      }
      else {
        return res.status(404).json({ error: 'Hashes Not Found' });
      }
    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};

export let getTransfers = (req: Request, res: Response, next: NextFunction) => {

  iota.api.getTransfers(SEED, (e, data) => {
    if (!e) {
      return res.status(200).json(data);
    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};

export let getAllTransfers = (req: Request, res: Response, next: NextFunction) => {

  const { address } = req.params;

  iota.api.findTransactionObjects({ addresses: [address] }, (e, data) => {
    if (!e) {
      return res.status(200).json(data);
    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};

export let getRecentTransfers = (req: Request, res: Response, next: NextFunction) => {

  const { address } = req.params;

  iota.api.findTransactionObjects({ addresses: [address] }, (e, data) => {
    if (!e) {

      data.sort((a, b) => {
        if (a.timestamp > b.timestamp) {
          return 1;
        }
        if (a.timestamp < b.timestamp) {
          return -1;
        }
        return 0;
      });

      data = data.slice(data.length - 11, data.length - 1);
      const newData: any[] = [];
      for (const dat of data) {
        newData.push({
          hash: dat.hash,
          message: iota.utils.extractJson([dat]),
        });
      }

      return res.status(200).json(newData);
    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};

export let accountData = (req: Request, res: Response, next: NextFunction) => {

  iota.api.getAccountData(SEED, (e, data) => {
    if (!e) {
      log.info('Account Data: ', data);
      return res.status(200).json(data);
    }
    else {
      return res.status(500).json({ error: e });
    }
  });

};
